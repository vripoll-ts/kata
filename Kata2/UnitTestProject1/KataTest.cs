﻿namespace UnitTestProject1
{
    using Kata2;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    [TestFixture]
    public class KataTest
    {
        private Kata _kata;

        [SetUp]
        protected void SetUp()
        {
            _kata = new Kata();
        }

        [Test]
        [TestCase(0, 2)]
        [TestCase(14, 7)]
        [TestCase(9, 3)]
        [TestCase(5, 5)]
        [TestCase(21, 3)]
        [TestCase(21, 7)]
        public void Kata_IsMultiple_Multiple_ReturnsTrue(int number, int divider)
        {
            Assert.IsTrue(_kata.IsMultiple(number, divider));
        }

        [Test]
        [TestCase(1, 3)]
        [TestCase(14, 8)]
        [TestCase(9, 4)]
        [TestCase(5, 6)]
        [TestCase(21, 5)]
        [TestCase(21, 4)]
        public void Kata_IsMultiple_NotMultiple_ReturnsFalse(int number, int divider)
        {
            Assert.IsFalse(_kata.IsMultiple(number, divider));
        }

        [Test]
        [TestCase(1, 3, 0)]
        [TestCase(14, 8, 0)]
        [TestCase(9, 9, 1)]
        [TestCase(555, 5, 3)]
        [TestCase(123456789, 2, 1)]
        [TestCase(212, 2, 2)]
        [TestCase(122, 2, 2)]
        [TestCase(10202, 2, 2)]
        public void Kata_GetOccurencesCount_WithNumberAndDivider_Ok(int number, int divider, int occurences)
        {
            Assert.AreEqual(occurences, _kata.GetOccurencesCount(number, divider));
        }

        [Test]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(4)]
        [TestCase(8)]
        [TestCase(62)]
        public void Kata_GetFooBarQixNumberParts_WithNoFooBarQix_ReturnsEmpty(int number)
        {
            var result = _kata.GetFooBarQixNumberParts(number);

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        [TestCase(6)]
        [TestCase(9)]
        [TestCase(12)]
        public void Kata_GetFooBarQixNumberParts_MultipleOf3Only_StartsWithFoo(int number)
        {
            var result = _kata.GetFooBarQixNumberParts(number);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Foo", result[0]);
        }

        [Test]
        [TestCase(10)]
        [TestCase(20)]
        [TestCase(40)]
        public void Kata_GetFooBarQixNumberParts_MultipleOf5Only_StartsWithBar(int number)
        {
            var result = _kata.GetFooBarQixNumberParts(number);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Bar", result[0]);
        }

        [Test]
        [TestCase(14)]
        [TestCase(28)]
        [TestCase(49)]
        public void Kata_GetFooBarQixNumberParts_MultipleOf7Only_IsQix(int number)
        {
            var result = _kata.GetFooBarQixNumberParts(number);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Qix", result[0]);
        }

        [Test]
        [TestCase(13)]
        [TestCase(23)]
        public void Kata_GetFooBarQixNumberParts_Containing3Only_IsFoo(int number)
        {
            var result = _kata.GetFooBarQixNumberParts(number);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Foo", result[0]);
        }

        [Test]
        [TestCase(52)]
        [TestCase(88856)]
        public void Kata_GetFooBarQixNumberParts_Containing5Only_IsBar(int number)
        {
            var result = _kata.GetFooBarQixNumberParts(number);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Bar", result[0]);
        }

        [Test]
        [TestCase(71)]
        [TestCase(88714)]
        public void Kata_GetFooBarQixNumberParts_Containing7Only_IsBar(int number)
        {
            var result = _kata.GetFooBarQixNumberParts(number);

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual("Qix", result[0]);
        }

        [Test]
        [TestCase(3 * 5)]
        [TestCase(3 * 5 * 7)]
        [TestCase(5 * 7)]
        [TestCase(3 * 7)]
        public void Kata_GetFooBarQixNumberParts_WithMultipleDividers_ReturnsFooBarQixInOrder(int number)
        {
            var result = _kata.GetFooBarQixMultipleParts(number);

            var firstFooIndex = FindIndex(result, r => r == "Foo");
            var firstBarIndex = FindIndex(result, r => r == "Bar");
            var firstQixIndex = FindIndex(result, r => r == "Qix");

            var indices = new[] { firstFooIndex, firstBarIndex, firstQixIndex }.Where(index => index >= 0).ToArray();

            // Assert
            if (indices.Length > 1)
            {
                for (int i = 1; i < indices.Length; ++i)
                {
                    Assert.IsTrue(indices[i] > indices[i - 1]);
                }
            }
        }

        [Test]
        [TestCase(35, "FooBar")]
        [TestCase(753, "QixBarFoo")]
        [TestCase(37, "FooQix")]
        [TestCase(57, "BarQix")]
        [TestCase(58703, "BarQixFoo")]
        public void Kata_GetFooBarQixNumberParts_WithAppearances_ReturnsFooBarQixInOrderOfAppearance(int number, string expected)
        {
            var result = string.Concat(_kata.GetFooBarQixContainingParts(number));

            Assert.AreEqual(expected, result);
        }

        [Test]
        [TestCase(1, "1")]
        [TestCase(2, "2")]
        [TestCase(3, "FooFoo")]
        [TestCase(4, "4")]
        [TestCase(5, "BarBar")]
        [TestCase(6, "Foo")]
        [TestCase(7, "QixQix")]
        [TestCase(8, "8")]
        [TestCase(9, "Foo")]
        [TestCase(51, "FooBar")]
        [TestCase(53, "BarFoo")]
        [TestCase(21, "FooQix")]
        [TestCase(13, "Foo")]
        [TestCase(15, "FooBarBar")]
        [TestCase(33, "FooFooFoo")]
        public void Kata_GetFooBarQixNumberParts_WithMultipleAndAppearances_ReturnsExpected(int number, string expected)
        {
            var result = _kata.GetFooBarQixNumber(number);

            Assert.AreEqual(expected, result);
        }

        public static int FindIndex<T>(IEnumerable<T> items, Func<T, bool> predicate)
        {
            int retVal = 0;
            foreach (var item in items)
            {
                if (predicate(item)) return retVal;
                retVal++;
            }
            return -1;
        }
    }
}
