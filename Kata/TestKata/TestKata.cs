﻿using Kata;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestKata
{
    [TestClass]
    public class TestKata
    {
        [TestMethod]
        public void Test_Generate_WithNegativeItemCount_Results_Empty()
        {
            // Arrange
            var expected = string.Empty;

            // Act
            var actual = Program.GetFizzBuzzString(-10);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_Generate_WithZeroItem_Results_Empty()
        {
            // Arrange
            var expected = string.Empty;

            // Act
            var actual = Program.GetFizzBuzzString(0);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_Generate_WithOneItem_Results_Number()
        {
            // Arrange
            var expected = "1\r\n";

            // Act
            var actual = Program.GetFizzBuzzString(1);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_Generate_WithThreeItem_Results_OneFizz()
        {
            // Arrange
            var expected = "1\r\n2\r\nFizz\r\n";

            // Act
            var actual = Program.GetFizzBuzzString(3);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_Generate_WithFiveItem_Results_OneFizz_OneBuzz()
        {
            // Arrange
            var expected = "1\r\n2\r\nFizz\r\n4\r\nBuzz\r\n";

            // Act
            var actual = Program.GetFizzBuzzString(5);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_Generate_WithFifteenItem_Results_OneFizzBuzz()
        {
            // Arrange
            var expected = "1\r\n2\r\nFizz\r\n4\r\nBuzz\r\nFizz\r\n7\r\n8\r\nFizz\r\nBuzz\r\n11\r\nFizz\r\n13\r\n14\r\nFizzBuzz\r\n";

            // Act
            var actual = Program.GetFizzBuzzString(15);

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
