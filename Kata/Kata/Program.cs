﻿using System;
using System.Globalization;
using System.Text;

namespace Kata
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.Write(GetFizzBuzzString(100));
        }

        /// <summary>
        /// Returns numbers from 1 to count as a string with a return line after each number, with multiples of 3 replaced by "Fizz", multiples of 5 replaced by "Buzz", 
        /// and multiples of 3 and 5 replaced by "FizzBuzz", or an empty string if count is 0 or less.
        /// </summary>
        /// <param name="count">The number of items output.</param>
        /// <returns>Numbers from 1 to count as a string with a return line after each number, with multiples of 3 replaced by "Fizz", multiples of 5 replaced by "Buzz", 
        /// and multiples of 3 and 5 replaced by "FizzBuzz", or an empty string if count is 0 or less.</returns>
        internal static string GetFizzBuzzString(int count)
        {
            var result = new StringBuilder();
            for (int i = 1; i <= count; ++i)
            {
                bool printNumber = true;
                if (i % 3 == 0)
                {
                    result.Append("Fizz");
                    printNumber = false;
                }
                if (i % 5 == 0)
                {
                    result.Append("Buzz");
                    printNumber = false;
                }
                if (printNumber)
                {
                    result.Append(i.ToString(CultureInfo.InvariantCulture));
                }
                result.AppendLine();
            }
            return result.ToString();
        }
    }
}
